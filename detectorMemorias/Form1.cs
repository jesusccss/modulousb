﻿using System;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.IO;
//-------
// QR
using Gma;
using Gma.QrCodeNet.Encoding;
using Gma.QrCodeNet.Encoding.Windows.Render;
using System.Drawing;
using System.Drawing.Imaging;
using System.Diagnostics;

namespace detectorMemorias
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();  
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            

        }
        //----------------------------------------------------------------------------------------------------
        // codigo genrador de QR
        //----------------------------------------------------------------------------------------------------
        private void button1_Click(object sender, EventArgs e)
        {
            crearCodigoQR(caja.Text.Trim());
        }
        private void crearCodigoQR(
            //string file_name, 
            string valor
            //, int image_size
            )
        {
//            string new_file_name = file_name;
            QrEncoder qrEncoder = new QrEncoder(ErrorCorrectionLevel.H);
            QrCode qrCode = new QrCode();
            qrEncoder.TryEncode(valor, out qrCode);

            GraphicsRenderer renderer = new GraphicsRenderer(
                new FixedCodeSize(400, QuietZoneModules.Zero),
                Brushes.Black,
                Brushes.White);
            MemoryStream ms = new MemoryStream();
            renderer.WriteToStream(qrCode.Matrix, ImageFormat.Png, ms);
            var imageTemp = new Bitmap(ms);
            var image = new Bitmap(imageTemp, new Size(new Point(200, 200)));
//            image.Save(new_file_name, ImageFormat.Png);
            pintura.Image = new System.Drawing.Bitmap(image);
//                pictura.Image = new System.Drawing.Bitmap(fullpath);
            //            return image; // 
            //            return new_file_name;
        }

        //----------------------------------------------------------------------------------------------------
        // codigo detectar conexion y desconexion de dispositivos
        public void detectarUSB() {
            listBox1.Items.Clear();// Borra el contenido
            DriveInfo[] allDrives = DriveInfo.GetDrives();
            char c = ':';
            string unidad = "";
            foreach (DriveInfo d in allDrives)
            {
                if(d.DriveType == DriveType.Removable)
                    listBox1.Items.Add("Unidad: " + d.Name + "  Tipo: " + d.DriveType);
                //listBox1.Items.Add("  Tipo: " + d.DriveType );
                //Console.WriteLine("Controlador {0}", d.Name);
                //Console.WriteLine("  Tipo de controlador: {0}", d.DriveType);
                if (
                    //d.IsReady == true &&
                    d.DriveType == DriveType.Removable)
                {
                    unidad = d.Name.Replace(":","").Replace("\\", "").Replace(">", "");
                    string comando = unidad;// + ":\\>vol";
                    listBox1.Items.Add("          Etiqueta: [" + d.VolumeLabel + "]  Formato: " + d.DriveFormat);
                    //listBox1.Items.Add("Sistema de archivos " + d.DriveFormat);
                    listBox1.Items.Add("          Espacio: " + d.TotalFreeSpace + "/" + d.TotalSize  );
                    listBox1.Items.Add("          serie: " + ejecutarComandoCMD(comando));

                    //listBox1.Items.Add("Espacio libre " + d.TotalFreeSpace);
                    //Console.WriteLine("  Etiqueta del dispositivo: {0}", d.VolumeLabel);
                    //Console.WriteLine("  Sistema de archivos: {0}", d.DriveFormat);
                    //Console.WriteLine("  Available space to current user:{0, 15} bytes", d.AvailableFreeSpace);

                    //Console.WriteLine(
                    //"  Total available space:          {0, 15} bytes",
                    //d.TotalFreeSpace);

                    //Console.WriteLine(
                    //"  Total size of drive:            {0, 15} bytes ",
                    //d.TotalSize);
                }
                listBox1.Items.Add("");
                listBox1.Items.Add("");
            }// end foreach

        }// end metodo detectarUSB

        static string ejecutarComandoCMD(string unidad){
            System.Diagnostics.Process proc = new System.Diagnostics.Process();
            proc.EnableRaisingEvents = false;
            proc.StartInfo.FileName = "cmd";
            proc.StartInfo.RedirectStandardInput = true ;
            proc.StartInfo.RedirectStandardOutput = true;
            proc.StartInfo.UseShellExecute = false;
            proc.StartInfo.CreateNoWindow = true;
            proc.Start();
            proc.StandardInput.WriteLine(unidad +":");
            proc.StandardInput.WriteLine("vol");
            proc.StandardInput.Flush();
            proc.StandardInput.Close();
            string result = proc.StandardOutput.ReadToEnd().ToString();
            proc.WaitForExit();
            proc.Close();
            string serie = result.Substring(result.Length - 17,11);
            return serie;
        }

        protected override void WndProc(ref Message m){
            switch (m.Msg){
                case Win32.WM_DEVICECHANGE: OnDeviceChange(ref m); break;
            }base.WndProc(ref m);
        }


        void OnDeviceChange(ref Message msg){
            int wParam = (int)msg.WParam;
            if (wParam == Win32.DBT_DEVICEARRIVAL) {
                label1.Text = "USB Conectada";
            }else if (wParam == Win32.DBT_DEVICEREMOVECOMPLETE) {
                label1.Text = "---";
            }detectarUSB();// ejecuta detectar USB
        }


        void RegisterHidNotification()
        {
            Win32.DEV_BROADCAST_DEVICEINTERFACE dbi = new Win32.DEV_BROADCAST_DEVICEINTERFACE();
            int size = Marshal.SizeOf(dbi);
            dbi.dbcc_size = size;
            dbi.dbcc_devicetype = Win32.DBT_DEVTYP_DEVICEINTERFACE;
            dbi.dbcc_reserved = 0;
            dbi.dbcc_classguid = Win32.GUID_DEVINTERFACE_HID;
            dbi.dbcc_name = 0;
            IntPtr buffer = Marshal.AllocHGlobal(size);
            Marshal.StructureToPtr(dbi, buffer, true);
            IntPtr r = Win32.RegisterDeviceNotification(Handle, buffer,
            Win32.DEVICE_NOTIFY_WINDOW_HANDLE);
            if (r == IntPtr.Zero) label1.Text = Win32.GetLastError().ToString();
        }


    }// end class
}// end namespace
